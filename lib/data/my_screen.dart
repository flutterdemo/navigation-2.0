const String UserPath = "/user";
const String UserDetail = "/userDetail";

class MyScreen{
  final String key;
  final String path;
  final ScreenDisplay screen;

  MyScreen(this.key, this.screen, this.path);
}

enum ScreenDisplay{
  USER, USER_DETAIL
}