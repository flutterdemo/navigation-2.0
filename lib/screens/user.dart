import 'package:flutter/material.dart';
import 'package:navigation/data/my_screen.dart';
import 'package:navigation/route/my_deligate.dart';

class UserScreen extends StatelessWidget {

  MyDeligate _valueChanged;

  UserScreen(this._valueChanged);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
       appBar: AppBar(title: Text("User List"),),
      body: Center(
        child: Container(
          child: InkWell(
            onTap: () {
              _valueChanged.addWidget(MyScreen("userDetail", ScreenDisplay.USER_DETAIL, UserDetail));
            },
            child: Text("Hit Me"),
          ),
        ),
      ),
    );
  }
}
