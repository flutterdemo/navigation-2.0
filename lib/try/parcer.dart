import 'package:flutter/cupertino.dart';
import 'package:navigation/try/screen_state.dart';

class MyScreenParcer extends RouteInformationParser<MyScreen> {
  @override
  Future<MyScreen> parseRouteInformation(RouteInformation routeInformation) {
    Uri uri = Uri.parse(routeInformation.location);
    print("${uri.pathSegments}");
    if (uri.pathSegments.isEmpty) {
      return Future.value(MAIN_SCREEN);
    }
    switch (uri.pathSegments[0]) {
      case "main":
        return Future.value(MAIN_SCREEN);
        break;
      case "details":
        return Future.value(DETAILS_SCREEN);
        break;
      default:
        return Future.value(MAIN_SCREEN);
        break;
    }
  }

  @override
  RouteInformation restoreRouteInformation(MyScreen configuration) {
    switch (configuration.screen) {
      case Screen.MAIN:
        return RouteInformation(location: MAIN_PATH);
        break;
      case Screen.DETAILS:
        return RouteInformation(location: DETAILS_PATH);
        break;
      default:
        return null;
        break;
    }
  }
}
