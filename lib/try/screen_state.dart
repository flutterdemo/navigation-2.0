class MyScreen {
  final String key;
  final Screen screen;
  final String path;

  MyScreen(this.key, this.screen, this.path);
}

enum Screen { MAIN, DETAILS }

const MAIN_PATH = "/main";
const DETAILS_PATH = "/details";

MyScreen MAIN_SCREEN = MyScreen("main", Screen.MAIN, MAIN_PATH);
MyScreen DETAILS_SCREEN = MyScreen("details", Screen.DETAILS, DETAILS_PATH);
