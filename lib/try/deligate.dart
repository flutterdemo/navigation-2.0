import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:navigation/try/scr/book.dart';
import 'package:navigation/try/scr/book_detail.dart';
import 'package:navigation/try/screen_state.dart';

class MyRouteDel extends RouterDelegate<MyScreen>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<MyScreen> {
  List<Page> _page = [];

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: _page,
      onPopPage: _pop,
    );
  }

  bool _pop(Route<dynamic> route, dynamic result) {
    final didPop = route.didPop(result);
    print("Pop $didPop");
    if (!didPop) {
      return false;
    }
    _page.remove(route.settings);
    notifyListeners();

    return didPop;
  }

  @override
  Future<bool> popRoute() {
    print("popRoute");
    if (_page.length > 1) {
      _page.removeLast();
      notifyListeners();
      return Future.value(true);
    }
    return Future.value(false);
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(MyScreen configuration) {
    print("setNewRoutePath ${configuration.screen}");
    _page.clear();
    checkAndAddPage(configuration);
    return Future.value();
  }

  @override
  MyScreen get currentConfiguration => _page.last.arguments as MyScreen;

  void checkAndAddPage(MyScreen configuration) {
    print("checkAndAddPage ${configuration.screen}");
    switch (configuration.screen) {
      case Screen.MAIN:
        _page.add(MaterialPage(
            child: Book(this),
            key: Key(configuration.key),
            arguments: configuration));
        break;
      case Screen.DETAILS:
        _page.add(MaterialPage(
            child: BookDetais(),
            key: Key(configuration.key),
            arguments: configuration));
        break;
      default:
        break;
    }

    notifyListeners();
  }

  void addPage(MyScreen openScreen) {
    checkAndAddPage(openScreen);
  }
}
