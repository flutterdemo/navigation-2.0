import 'package:flutter/material.dart';
import 'package:navigation/try/deligate.dart';
import 'package:navigation/try/screen_state.dart';

class Book extends StatelessWidget {
  MyRouteDel _del;

  Book(this._del);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Book List"),
      ),
      body: Center(
        child: RaisedButton(
            onPressed: () {
              _del.addPage(DETAILS_SCREEN);
            },
            child: Text("Read me")),
      ),
    );
  }
}
