import 'package:flutter/cupertino.dart';
import 'package:navigation/try/deligate.dart';

class BackDiapacher extends RootBackButtonDispatcher {
  MyRouteDel _del;
  BackDiapacher(this._del) : super();

  @override
  Future<bool> didPopRoute() {
    return _del.popRoute();
  }
}
