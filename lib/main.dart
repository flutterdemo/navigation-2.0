import 'package:flutter/material.dart';
import 'package:navigation/data/my_screen.dart';
import 'package:navigation/route/my_deligate.dart';
import 'package:navigation/route/my_parser.dart';


void main() {
  runApp(MyHome());
}

class MyHome extends StatelessWidget {

  MyDeligate _del = MyDeligate();
  MyRouteParser _parser = MyRouteParser();

  @override
  Widget build(BuildContext context) {
    _del.setNewRoutePath(MyScreen("user", ScreenDisplay.USER, UserPath));
    return MaterialApp.router(
        routeInformationParser: _parser,
        routerDelegate: _del);
  }
}



/*class ForMe extends StatelessWidget {
  MyRouteDel _del = MyRouteDel();
  MyScreenParcer _par = MyScreenParcer();
  BackDiapacher _back;

  @override
  Widget build(BuildContext context) {
    _del.setNewRoutePath(MAIN_SCREEN);
    _back = BackDiapacher(_del);
    return MaterialApp.router(
      routeInformationParser: _par,
      routerDelegate: _del,
      backButtonDispatcher: _back,
    );
  }
}*/
