import 'package:flutter/cupertino.dart';
import 'package:navigation/data/my_screen.dart';

class MyRouteParser extends RouteInformationParser<MyScreen>{

  @override
  Future<MyScreen> parseRouteInformation(RouteInformation routeInformation) async{
    Uri uri = Uri.parse(routeInformation.location);
    print("Path Segment ${uri.pathSegments}");
    if(uri.pathSegments.isEmpty){
       return Future.value(MyScreen("user", ScreenDisplay.USER, UserPath));
    }

    switch(uri.pathSegments[0]){
      case "user":
        return Future.value(MyScreen("user", ScreenDisplay.USER, UserPath));
        break;
      case "userDetail":
        return Future.value(MyScreen("userDetail", ScreenDisplay.USER_DETAIL, UserDetail));
        break;
    }


    return null;
  }

  @override
  RouteInformation restoreRouteInformation(MyScreen configuration) {
    print("restoreRouteInformation ${configuration.screen}");
    switch(configuration.screen){
      case ScreenDisplay.USER:
        return RouteInformation(location: UserPath);
        break;
      case ScreenDisplay.USER_DETAIL:
        return RouteInformation(location: UserDetail);
        break;
      default :
        break;
    }

    return null;
  }

}