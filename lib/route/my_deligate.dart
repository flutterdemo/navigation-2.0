import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:navigation/data/my_screen.dart';
import 'package:navigation/screens/user.dart';
import 'package:navigation/screens/user_details.dart';

class MyDeligate extends RouterDelegate<MyScreen> with ChangeNotifier, PopNavigatorRouterDelegateMixin<MyScreen>{

  List<Page> _pages = [];

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: _pages,
      onPopPage: _pop,
    );
  }

  bool _pop(Route<dynamic> route, result){
    bool isPop = route.didPop(result);
    if(!isPop){
      return false;
    }

    if(_pages.length > 1){
      _pages.removeLast();
      notifyListeners();
    }
    return isPop;
  }

  @override
  GlobalKey<NavigatorState> get navigatorKey => GlobalKey<NavigatorState>();

  @override
  Future<void> setNewRoutePath(MyScreen configuration) {
      _pages.clear();
      checkAndMove(configuration);
      return Future.value(null);
  }

  @override
  MyScreen get currentConfiguration => _pages.last.arguments as MyScreen;

  void checkAndMove(MyScreen configuration) {
     switch(configuration.screen){
       case ScreenDisplay.USER:
         _pages.add(MaterialPage(child: UserScreen(this), key: Key(configuration.key), arguments: configuration));
         break;
       case ScreenDisplay.USER_DETAIL:
         _pages.add(MaterialPage(child: UserDetails(), key: Key(configuration.key), arguments: configuration));
         break;
       default:
         break;
     }
     notifyListeners();
  }

  void addWidget(MyScreen myScreen) {
    checkAndMove(myScreen);
  }
}